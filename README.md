# Random Item Picker



## Development server

### Running against Dev
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Running against local server
1. Run `json-server --watch src/assets/db.json --routes src/assets/routes.json --fks _id` to create a local server using test data
1. Run `ng serve -c local -o` to run app against the local server.
