import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { TextfieldComponent } from './textfield/textfield.component';
import { RandomComponent } from './random/random.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { OptionCardComponent } from './option-card/option-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OptionComponent } from './option/option.component';
import { OptionDetailsComponent } from './option-details/option-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { TodoListComponent } from './todo-list/todo-list.component';

/* Add Amplify imports */
import { AmplifyUIAngularModule } from '@aws-amplify/ui-angular';
import Amplify from '@aws-amplify/auth';
import { environment } from '../environments/environment';
import { TokenInterceptor } from './http-interceptors/token-intercepter';
import { httpInterceptorProviders } from './http-interceptors';

/* Configure Amplify resources */
Amplify.configure(environment.awsconfig);

@NgModule({
	declarations: [
		AppComponent,
		ListComponent,
		TextfieldComponent,
		RandomComponent,
		HeaderComponent,
		FooterComponent,
		OptionCardComponent,
		OptionComponent,
		OptionDetailsComponent,
		TodoListComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FontAwesomeModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatExpansionModule,
		MatButtonModule,
		MatIconModule,
		MatToolbarModule,
		MatFormFieldModule,
		MatInputModule,
		MatDialogModule,
		MatSnackBarModule,
		MatSelectModule,
		MatMenuModule,
		ReactiveFormsModule,
		FormsModule,
		AmplifyUIAngularModule
	],
	providers: [
		httpInterceptorProviders
	],
	bootstrap: [AppComponent],
	entryComponents: [
		OptionDetailsComponent,
	],
})
export class AppModule { }
