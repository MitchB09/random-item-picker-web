import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { TextFieldMode } from './textfield-mode';
import { faCheck, faEdit, faTimes, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-textfield',
	templateUrl: './textfield.component.html',
	styleUrls: ['./textfield.component.scss']
})
export class TextfieldComponent implements OnInit {

	@Input() text: string;
	@Input() checkable = false;
	@Input() editable = true;
	@Input() placeholder = 'Placeholder Text';
	@Input() checked = false;

	@Output() textChange = new EventEmitter<string>();
	@Output() checkedChange = new EventEmitter<boolean>();

	@ViewChild('newText', {static: false}) newText: ElementRef;

	currentMode: TextFieldMode;
	TextFieldMode: TextFieldMode;
	faCheck = faCheck;
	faEdit = faEdit;
	faTimes = faTimes;
	faTimesCircle = faTimesCircle;

	constructor() {

	}

	ngOnInit() {
		if (this.checkable && this.checked) {
			this.currentMode = TextFieldMode.CHECKED;
		} else {
			this.currentMode = TextFieldMode.DISPLAY;
		}
	}

	updateText() {
		this.text = this.newText.nativeElement.value;
		this.currentMode = TextFieldMode.DISPLAY;
		this.textChange.emit(this.text);
	}

	makeEditable() {
		if (this.editable) {
			this.currentMode = TextFieldMode.EDIT;
			setTimeout(() => { // this will make the execution after the above boolean has changed
				this.newText.nativeElement.focus();
			}, 0);
		}
	}

	cancel() {
		this.currentMode = TextFieldMode.DISPLAY;
	}

	checkItem() {
		if (this.checked) {
			this.checked = false;
			this.currentMode = TextFieldMode.DISPLAY;
		} else {
			this.checked = true;
			this.currentMode = TextFieldMode.CHECKED;
		}
		this.checkedChange.emit(this.checked);
	}

	onKeyDown(event) {
		if (event.key === 'Enter') {
			this.updateText();
		} else if (event.key === 'Escape') {
			this.cancel();
		}
	}
}
