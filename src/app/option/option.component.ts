import { Component, OnInit, Input } from '@angular/core';
import { Option } from '../option';
import { OptionService } from '../option.service';
import { ActivatedRoute } from '@angular/router';
import { ItemService } from '../item.service';
import { v1 as uuidv1 } from 'uuid';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-option',
	templateUrl: './option.component.html',
	styleUrls: ['./option.component.scss', '../app.component.scss']
})
export class OptionComponent implements OnInit {

	@Input() option: Option;
	newItem: string;
	faTimesCircle = faTimesCircle;

	constructor(
		private optionService: OptionService,
		private itemService: ItemService,
		private route: ActivatedRoute,
		private snackBar: MatSnackBar,
		private router: Router) { }

	ngOnInit() {
		let listName;
		if (this.option && this.option.listName) {
			listName = this.option.listName;
		} else {
			this.route.paramMap.subscribe(params => {
				listName = params.get('id');
			});
		}
		this.optionService.getOption(listName).subscribe(data => {
			this.option = data;
		});
	}

	addItem(newItem: string) {
		if (!this.option.items) {
			this.option.items = [];
		}

		this.option.items.push({id: uuidv1(), value: newItem});
		this.newItem = '';
	}

	deleteItem(itemId: string) {
		const removeIndex = this.option.items.map( (item) =>  item.id).indexOf(itemId);
		if (removeIndex !== -1) {
			this.option.items.splice(removeIndex, 1);
		}
	}

	updateOption() {
		this.optionService.updateOption(this.option).subscribe(data => {
			this.snackBar.open('Saved Successfully', 'close', {
				duration: 2000,
			});
		});
	}

	deleteOption() {
		this.optionService.deleteOption(this.option).subscribe(data => {
			this.snackBar.open('Successfully Deleted Category', 'close', {
				duration: 2000,
			});
			this.router.navigate(['']);
		});
	}

	// TODO bilenm - update local object
	updateItem(item) {
		this.itemService.updateItem(this.option.listName, item);
	}
}
