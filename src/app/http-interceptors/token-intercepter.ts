import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { Observable, from } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { Auth } from '@aws-amplify/auth';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

	constructor() { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return from(Auth.currentSession())
		.pipe(
				switchMap((auth: any) => { // switchMap() is used instead of map().
						const jwt = auth.idToken.jwtToken;
						const withAuthRequest = req.clone({
								setHeaders: {
										Authorization: `${jwt}`
								}
						});
						return next.handle(withAuthRequest);
				}),
				catchError((err) => {
						return next.handle(req);
				})
		);
	}
}
