import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Option } from './option';
import { environment } from 'src/environments/environment.js';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class OptionService {

	url = `${environment.baseUrl}/lists`;

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json; charset=UTF-8'
		})
	};

	constructor(
		private http: HttpClient,
	) { }

	getOptions(): Observable<Option[]> {
		return this.http.get<Option[]>(this.url, this.httpOptions);
	}

	getOption(id: string): Observable<Option> {
		return this.http.get<Option>(`${this.url}/${id}`, this.httpOptions);
	}

	getRandomItemInOption(option) {
		if (option && option.items) {
			const items = option.items;
			return items[Math.floor(Math.random() * Math.floor(items.length))];
		} else {
			return 'Nothing to chose from!';
		}
	}

	createOption(option: Option) {
		return this.http.post(`${this.url}`, option, this.httpOptions);
	}

	updateOption(option: Option) {
		return this.http.put(`${this.url}/${option.listName}`, option, this.httpOptions);
	}

	deleteOption(option: Option) {
		return this.http.delete(`${this.url}/${option.listName}`, this.httpOptions);
	}
}
