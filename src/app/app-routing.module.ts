import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { RandomComponent } from './random/random.component';
import { OptionComponent } from './option/option.component';


const routes: Routes = [
	{ path: '', component: ListComponent },
	{ path: 'todo', component: TodoListComponent },
	{ path: ':id', component: OptionComponent },
	{ path: ':id/random', component: RandomComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
