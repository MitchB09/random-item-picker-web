import { Injectable } from '@angular/core';
import { Item } from './item';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class ItemService {

	url = `${environment.baseUrl}/lists`;

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json; charset=UTF-8'
		})
	};

	constructor(private http: HttpClient) { }

	getItemsByOptionId(optionId: string) {
		return this.http.get(`${this.url}/${optionId}/items`, this.httpOptions);
	}

	createItem(optionId: string, item: Item) {
		return this.http.post(`${this.url}/${optionId}/items`, item, this.httpOptions);
	}

	updateItem(optionId: string, item: Item) {
		return this.http.put(`${this.url}/${optionId}/items/${item.id}`, item, this.httpOptions);
	}

	deleteItem(optionId: string, item: Item) {
		return this.http.delete(`${this.url}/${optionId}/items/${item.id}`, this.httpOptions);
	}
}
