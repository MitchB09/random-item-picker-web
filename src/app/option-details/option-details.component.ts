import { Component, OnInit, Input, Inject } from '@angular/core';
import { Option } from '../option';
import { OptionService } from '../option.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ListType } from '../listType';
import { v1 as uuidv1 } from 'uuid';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
	selector: 'app-option-details',
	templateUrl: './option-details.component.html',
	styleUrls: ['./option-details.component.scss']
})
export class OptionDetailsComponent implements OnInit {

	@Input() option: Option = new Option();
	ListType: ListType;
	creatingNew = false;

	ngOnInit() {

	}

	constructor(
		public dialogRef: MatDialogRef<OptionDetailsComponent>,
		private optionService: OptionService,
		@Inject(MAT_DIALOG_DATA) public data: Option,
		private snackBar: MatSnackBar,
		private router: Router) {
			this.option = data;
			if (!this.option.listName) {
				this.creatingNew = true;
			}
	}

	public get listTypes(): typeof ListType {
		return ListType;
	}

	saveOption() {
		this.optionService.createOption(this.option).subscribe(data => {
			this.dialogRef.close();
			if (this.creatingNew) {
				this.router.navigate([this.option.listName]);
			} else {
				this.snackBar.open('Saved Successfully', 'close', {
					duration: 2000,
				});
			}
		});
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

}
