import { Component, OnInit } from '@angular/core';
import { OptionService } from '../option.service';
import { Item } from '../item';
import { ActivatedRoute } from '@angular/router';
import { Option } from '../option';

@Component({
	selector: 'app-random',
	templateUrl: './random.component.html',
	styleUrls: ['./random.component.scss', '../app.component.scss']
})
export class RandomComponent implements OnInit {

	id: string;
	option: Option;
	notificationPermission: string = Notification.permission;
	randomItem: Item;
	notificationInterval = 30;
	isLoaded = false;

	constructor(
		private optionService: OptionService,
		private route: ActivatedRoute) { }

	getRandomItem() {
		this.randomItem = this.optionService.getRandomItemInOption(this.option);
		return this.randomItem;
	}

	ngOnInit() {
		this.route.paramMap.subscribe(params => {
			this.id = params.get('id');
			this.optionService.getOption(this.id).subscribe(data => {
				this.option = data;
				this.isLoaded = true;
				this.getRandomItem();
			});

		})
		;
	}

	setupNotifications() {
		Notification.requestPermission().then((result) => {
			console.log(result);
		});

		if (!('Notification' in window)) {
				console.log('This browser does not support notifications.');
		} else {
			Notification.requestPermission()
				.then((permission) => {
					this.notificationPermission = permission;
				});
		}
	}

	createNotification() {
		const img = '/to-do-notifications/img/icon-128.png';
		const text = 'New Workout: "' + this.randomItem.value + '"!';
		const notification = new Notification('Random Workout Picker', { body: text, icon: img });
	}

	loopNotifications() {
		setInterval(() => {
			this.getRandomItem();
			this.createNotification();
		}, this.notificationInterval * 60 * 1000); // 30 * 60 * 1000 milsec = 30 minutes
	}
}
