export enum ListType {
	RandomList = "Random Generator",
	TodoList = "Todo List",
	RecurringNotification = "Recurring Notification"
}

