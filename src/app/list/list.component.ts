import { Component, OnInit } from '@angular/core';
import { OptionService } from '../option.service';
import { faPlus  } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { OptionDetailsComponent } from '../option-details/option-details.component';
import { Option } from '../option';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss', '../app.component.scss']
})
export class ListComponent implements OnInit {

	optionList = [];
	faPlus = faPlus;

	constructor(
		private optionService: OptionService,
		public dialog: MatDialog) { }

	ngOnInit() {
		this.getOptions();
	}

	getOptions() {
		this.optionService.getOptions().subscribe((data) => {
			this.optionList = data;
		});
	}

	openOptionDialog(): void {
		const dialogRef = this.dialog.open(OptionDetailsComponent, {
			width: '250px',
			data: new Option()
		});

		dialogRef.afterClosed().subscribe(result => {
			console.dir(result);
		});
	}

	createOption(listName: string) {

	}


	updateOption(option) {

	}

	deleteOption(option) {

	}
}
