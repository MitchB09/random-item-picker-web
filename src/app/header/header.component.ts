import { Component, OnInit } from '@angular/core';
import { Auth } from '@aws-amplify/auth';
import { faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	faUser = faUser;
	userData: object;

	constructor() {
		Auth.currentUserInfo().then((data) => {
			this.userData = data.attributes;
			console.dir(this.userData);
		});
	 }

	ngOnInit() {
	}

	signOut() {
		Auth.signOut()
			.then((data) => {
				console.dir(data);
			}).catch(err => {
				console.dir('Error: ' + err);
			});
	}

}
