import { Component, OnInit } from '@angular/core';
import { version } from 'src/environments/version';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

	npmVersion = version.version;
	commitSHA = version.revision;
	branchName = version.branch;

	constructor() {

	}

	ngOnInit() {
	}

}
