import { Component, OnInit, Input } from '@angular/core';
import { Option } from '../option';
import { MatDialog } from '@angular/material/dialog';
import { OptionDetailsComponent } from '../option-details/option-details.component';
import { OptionService } from '../option.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
	selector: 'app-option-card',
	templateUrl: './option-card.component.html',
	styleUrls: ['./option-card.component.scss', '../app.component.scss']
})
export class OptionCardComponent implements OnInit {

	@Input() option: Option;
	@Input() expanded = false;

	constructor(
		public dialog: MatDialog,
		private optionService: OptionService,
		private snackBar: MatSnackBar) {

	}

	ngOnInit() {
	}

	editCategory(): void {
		const dialogRef = this.dialog.open(OptionDetailsComponent, {
			width: '250px',
			data: this.option
		});
	}

	deleteList() {
		this.optionService.deleteOption(this.option).subscribe(data => {
			this.snackBar.open('Deleted Successfully', 'close', {
				duration: 2000,
			});
		});
	}

}
