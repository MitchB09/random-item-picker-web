import { Item } from './item';
import { ListType } from './listType';

export class Option {
	id: string;
	listName: string;
	type: ListType;
	listDescription: string;
	items: Item[];
}
