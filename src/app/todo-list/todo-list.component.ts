import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { OptionService } from '../option.service';
import { MatDialog } from '@angular/material/dialog';
import { OptionDetailsComponent } from '../option-details/option-details.component';

@Component({
	selector: 'app-todo-list',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

	optionList = [];
	faPlus = faPlus;

	constructor(
		private optionService: OptionService,
		public dialog: MatDialog) { }

	ngOnInit() {
		this.getOptions();
	}

	getOptions() {
		this.optionService.getOptions().subscribe((data) => {
			this.optionList = data;
		});
	}

	openOptionDialog(): void {
		const dialogRef = this.dialog.open(OptionDetailsComponent, {
			width: '250px',
			data: new Option()
		});

		dialogRef.afterClosed().subscribe(result => {
			console.dir(result);
		});
	}

	createOption(listName: string) {

	}


	updateOption(option) {

	}

	deleteOption(option) {

	}
}
