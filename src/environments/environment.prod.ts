export const environment = {
	production: true,
	baseUrl: 'https://xu2cjqvw29.execute-api.us-east-1.amazonaws.com/dev',
	awsconfig: {
		Auth: {
			mandatorySignIn: true,
			region: 'us-east-1',
			userPoolId: 'us-east-1_y7ud5CKe2',
			userPoolWebClientId: 'm8pj9ja8hia97beh85rh6mofn'
		}
	}
};
