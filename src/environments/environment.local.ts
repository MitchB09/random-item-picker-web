export const environment = {
	production: false,
	baseUrl: 'http://localhost:3000',
	apiKey: 'FakeApiKey',
	aws_exports: {
		Auth: {
			mandatorySignIn: true,
			region: 'us-east-1',
			userPoolId: 'us-east-1_y7ud5CKe2',
			userPoolWebClientId: 'm8pj9ja8hia97beh85rh6mofn'
		}
	}
};

